#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 16:31:35 2017

@author: mauriceg
#fill_0_folder
##Script to create the 0 folder from the constant/polyMesh/boundary
    * usage:
        python fill_0_folder.py case_directory fields 
    * example: 
        python fill_0_folder.py /data1/CFD/forwstep_rhocentral/ U p

"""
import os 
import sys

fields = []
case_dir=sys.argv[1]
for n in range(2,len(sys.argv)):
    fields.append(sys.argv[n])
try:
    f = open(case_dir + '/constant/polyMesh/boundary')
except:
    print('create the mesh first'.capitalize())
    raise
    
    
        
fi = f.read()
f.close()

fi = fi.split('\n')


for n,line in enumerate(fi):
    if line.isdecimal():
        print('first bouandary:',fi[n+2])
        n=n+2
        break

fi = fi[n:]
boundaries = []
types = []
for n, line in enumerate(fi):
    c_line =  list(line)
    if '{' in c_line:
        boundaries.append(fi[n-1])
        types.append(fi[n+1].split())


print(boundaries)
print(types)
if not(os.path.exists('0')):
    os.mkdir('0')
    
for field in fields:
    f = open('data/header_' + field)
    head = f.read()
    f.close()
    f=open('0/'+field,'w')
    f.write(head)
    f.write('\n')
    f.write('boundaryField\n{\n')
    for n,boundary in enumerate(boundaries):
        f.write('    '+boundary+'\n')
        f.write('    {\n')
        f.write('        '+'type' + '    '+types[n][1]+'\n')
        f.write('    }\n')
        f.write('\n')
    f.write('}')
    f.close()

